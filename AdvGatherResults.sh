###################################
# you need modify the below part

# YMVA XCB XDB
Ver=XDB
LEP=0

####################################
mkdir Results_"$Ver"_"$LEP"

# copy prefit and postfit plots
# check if you only get one result with:
# ls plots/*_fullRes_VHbbRun2_13TeV_"$Ver."01_"$LEP"_125_Systs_use1tagFalse_*
# replace "$Ver." and "$LEP" with your fitting
echo "collecte plots!"
sleep 1s
cp -r plots/*_fullRes_VHbbRun2_13TeV_"$Ver."01_"$LEP"_125_Systs_use1tagFalse_*/* Results_"$Ver"_"$LEP"/
echo "done!"

# create the prefit yields table
echo "collecte yields!"
sleep 1s
mkdir -p Results_"$Ver"_"$LEP"/Yields
mv Results_"$Ver"_"$LEP"/events.txt Results_"$Ver"_"$LEP"/Yields/
echo "done!"

# collect the breakdown
echo "collecte breakdown!"
sleep 1s
mv Results_"$Ver"_"$LEP"/breakdown/ Results_"$Ver"_"$LEP"/breakdown_asimov/
echo "done!"

# collect significance
echo "collecte significance!"
sleep 1s
mkdir -p Results_"$Ver"_"$LEP"/Significance/
echo "There should be 5 lines in this file (this line included), otherwise ask producer!" > Results_"$Ver"_"$LEP"/Significance/Sig.txt
echo "Exp. Significance prefit asimov:" >> Results_"$Ver"_"$LEP"/Significance/Sig.txt
grep "Median significance" logs/*_fullRes_VHbbRun2_13TeV_"$Ver."01_"$LEP"*_getSig_125.log >> Results_"$Ver"_"$LEP"/Significance/Sig.txt
echo "Exp. Significance postfit asimov:" >> Results_"$Ver"_"$LEP"/Significance/Sig.txt
grep "Median significance" logs/*_fullRes_VHbbRun2_13TeV_"$Ver."02_"$LEP"*_getSig_125.log >> Results_"$Ver"_"$LEP"/Significance/Sig.txt
echo "done!"

# collect the rankings
echo "collecte rankings!"
sleep 1s
mkdir -p Results_"$Ver"_"$LEP"/Rankings_Asimov_postfit_SF

cp pdf-files/*"$Ver."01_"$LEP"*pulls_125.pdf Results_"$Ver"_"$LEP"/Rankings_Asimov_postfit_SF/
cp png-files/*"$Ver."01_"$LEP"*pulls_125.png Results_"$Ver"_"$LEP"/Rankings_Asimov_postfit_SF/
cp eps-files/*"$Ver."01_"$LEP"*pulls_125.eps Results_"$Ver"_"$LEP"/Rankings_Asimov_postfit_SF/

mkdir -p Results_"$Ver"_"$LEP"/Rankings_Asimov_prefit_SF

cp pdf-files/*"$Ver."03_"$LEP"*pulls_125.pdf Results_"$Ver"_"$LEP"/Rankings_Asimov_prefit_SF/
cp png-files/*"$Ver."03_"$LEP"*pulls_125.png Results_"$Ver"_"$LEP"/Rankings_Asimov_prefit_SF/
cp eps-files/*"$Ver."03_"$LEP"*pulls_125.eps Results_"$Ver"_"$LEP"/Rankings_Asimov_prefit_SF/

echo "done!"
# convert eps into png/pdf, needed to create the webpage
#echo "convert the eps to png/pdf for shapes and systs"
#echo "please DO NOT interrupt!"
#sleep 3s
#cd $WORKDIR/Results_"$Ver"_"$LEP"/shapes
#source $WORKDIR/macros/webpage/convertepstopngpdf.sh 
#cd $WORKDIR/Results_"$Ver"_"$LEP"/systs
#source $WORKDIR/macros/webpage/convertepstopngpdf.sh 
#cd $WORKDIR
#echo "done!"

# pack it up
tar zcf Results_"$Ver"_"$LEP".tar.gz Results_"$Ver"_"$LEP"

echo "The results collection is done!"
#echo "You need add NP comparison manually!"
