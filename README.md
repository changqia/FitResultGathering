With your dedicated settings of your own channels = [“0”]/doCutBase = False/doDiboson = False, 

you could running the WSM with the following setting with tags so that the GatherResults script could work with your results.

You run the job twice, you can modify YMVA to whatever you want, but 01 and 02 should be kept:

python launch_default_job.sh YMVA.01 python launch_default_job.sh YMVA.02 

——————————————

when Arguments ==YMVA.01 you set:

  doExp = "1" # "0" to run observed, "1" to run expected only
  
  # What to run

  createSimpleWorkspace = True

  runPulls = True

  runBreakdown = True

  runRanks = True

  runLimits = False

  runP0 = True

  runToyStudy = False
  
  # Turn on additional debug plots

  doftonly=False

  doplots=True

  # to use the postfit asimov dataset to calculate the rankings (which is default)

  runRankingOnPostfitAsimov = True

——————————————

when Arguments ==YMVA.02 you set:

  doExp = "0" # "0" to run observed, "1" to run expected only
  
  # What to run

  createSimpleWorkspace = True

  runPulls = False

  runBreakdown = False

  runRanks = False

  runLimits = False

  runP0 = True

  runToyStudy = False
  
  # Turn on additional debug plots

  doftonly = True

  doplots = False

——————————————

when Arguments ==YMVA.03 you set:

  doExp = "1" # "0" to run observed, "1" to run expected only
  
  # What to run

  createSimpleWorkspace = True

  runPulls = False

  runBreakdown = False

  runRanks = True

  runLimits = False

  runP0 = False

  runToyStudy = False
  
  # Turn on additional debug plots

  doftonly = True

  doplots = False

  # to use the prefit asimov dataset to calculate the rankings

  runRankingOnPostfitAsimov = False

——————————————

modify AdvGatherResults.sh 

Ver=YMVA

LEP=0 # the lepton channel and run:

source AdvGatherResults.sh

you will get Results_YMVA_0.tar.gz

—————————————————————————————————————————

DO NOT forget to set vh_fit=False when you run diboson fit in:

scripts/VHbbRun2/analysisPlottingConfig.py
